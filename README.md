
                                             Running of Code

      To run the code there are some  prerequisite
            1-Python 2.7
            2-Django 1.8
            3-postman
            4- Sql lite

      Functionalities:-
      . User can get information of orders happen within a today
      . user can get top 5 diets that was ordered.
      .user can get information about total_cal, total_fat, total_protein taken by a user.

      API:-
      . for getting the information of orders happen within a day http://127.0.0.1:8000/health/userorder/
      . for top 5 orders http://127.0.0.1:8000/health/toporder/
      . for total_cal & total_fat &total_protein taken by a user http:127.0.0.1:8000/health/totaldiet

      How to run project:-
      To run the code just open terminal and go to the directory where the code folder is there .
      Run server by using command [python manage.py runserver]
      There are certain urls provided to run certain urls as mentioned above.
