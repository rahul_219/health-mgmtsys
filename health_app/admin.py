from django.contrib import admin

from models import UserData
from models import DietInfo
from models import Order

admin.site.register(UserData)
admin.site.register(DietInfo)
admin.site.register(Order)
# Register your models here.
