# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DietInfo',
            fields=[
                ('diet_name', models.CharField(max_length=55, serialize=False, primary_key=True)),
                ('diet_cal', models.IntegerField(default=0)),
                ('diet_protein', models.IntegerField(default=0)),
                ('diet_fat', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('time', models.DateTimeField(auto_now_add=True, null=True)),
                ('order_no', models.IntegerField(serialize=False, primary_key=True)),
                ('diet', models.ManyToManyField(related_name='user_diet', to='health_app.DietInfo')),
            ],
        ),
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('name', models.CharField(max_length=55, serialize=False, primary_key=True)),
                ('d_o_b', models.DateField()),
                ('weight', models.IntegerField(default=50)),
                ('height', models.IntegerField(default=130)),
            ],
        ),
        migrations.AddField(
            model_name='order',
            name='user',
            field=models.ForeignKey(to='health_app.UserData'),
        ),
    ]
