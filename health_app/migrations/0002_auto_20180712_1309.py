# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('health_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='diet',
            field=models.ManyToManyField(to='health_app.DietInfo'),
        ),
    ]
