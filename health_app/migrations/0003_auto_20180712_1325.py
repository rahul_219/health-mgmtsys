# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('health_app', '0002_auto_20180712_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dietinfo',
            name='diet_cal',
            field=models.IntegerField(default='null'),
        ),
        migrations.AlterField(
            model_name='dietinfo',
            name='diet_fat',
            field=models.IntegerField(default='null'),
        ),
        migrations.AlterField(
            model_name='dietinfo',
            name='diet_protein',
            field=models.IntegerField(default='null'),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='height',
            field=models.IntegerField(default='null'),
        ),
        migrations.AlterField(
            model_name='userdata',
            name='weight',
            field=models.IntegerField(default='null'),
        ),
    ]
