from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
#from django.utils import datetime
from datetime import datetime

# Create your models here.
class UserData(models.Model):
    class Meta:
        verbose_name_plural = "users_data"
    name = models.CharField(max_length=55, primary_key = True)
    d_o_b = models.DateField()
    weight = models.IntegerField(default= 'null')
    height = models.IntegerField(default= 'null')
    #dietinfo = models.ForeignKey('DietInfo',on_delete = models.CASCADE)


    def __str__(self):
        return (self.name)

class DietInfo(models.Model):
    class Meta:
        verbose_name_plural = "diets_info"
    diet_name = models.CharField(max_length = 55, primary_key = True)
    diet_cal = models.IntegerField(default = 'null')
    diet_protein = models.IntegerField(default = 'null')
    diet_fat = models.IntegerField(default = 'null')

    def __str__(self):
        return (self.diet_name)



class Order(models.Model):
    class Meta:
        verbose_name_plural = "orders_info"
    time = models.DateTimeField(auto_now_add = True, null = True)
    order_no = models.IntegerField(primary_key = True)
    # AutoField()
    user = models.ForeignKey('UserData')
    diet = models.ManyToManyField('DietInfo')

    def __str__(self):
        return str(self.order_no)
