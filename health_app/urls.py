from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from health_app.views import OrderList,TopOrder,TotalDiet

urlpatterns = [
	#url(r'^sum/$', csrf_exempt(testapp.as_view()),name="test-sum"),
	url(r'^userorder', OrderList.as_view()),
    url(r'^toporder', TopOrder.as_view()),
	url(r'^totaldiet', TotalDiet.as_view()),
	#url(r'^delete_req/(?P<id>\d+)$', DeleteData.as_view(),name='l'),
]
