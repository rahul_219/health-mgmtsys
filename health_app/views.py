from django.shortcuts import render
from health_app.models import UserData
from health_app.models import DietInfo
from health_app.models import Order
from django.core.exceptions import ObjectDoesNotExist,MultipleObjectsReturned
from django.core import serializers
from django.views.generic import View
from django.http import HttpResponse, QueryDict
import datetime
import json
from django.http import JsonResponse
from collections import Counter

# Create your views here.

#list of orders happend within a day
class OrderList(View):
    def get(self,request):
        try:
            date = request.GET.get('Date')
            answer = []
            obj = Order.objects.filter(time__contains=date)
            if not obj:
                return HttpResponse('Date does not exist', status=400)
            for val in obj:
                ans = {}
                order_list = []
                ans['order_id'] = str(val.order_no)
                ans['time'] = str(val.time)
                ans['user'] = val.user.name
                obj = Order.objects.get(order_no = val.order_no)
                diet_names = obj.diet.all()
                for j in diet_names:
                    order_list.append(j.diet_name)
                ans['order_list'] = order_list
                answer.append(ans)
                return HttpResponse(json.dumps(answer), content_type='application/json', status=200)
        except Order.DoesNotExist, e:
            return HttpResponse('No order at this date', status=400)
        except Exception, e:
            return HttpResponse('Internal Server', status=400)


    def post(self,request):
        try:
            name= request.POST['name']
            d_o_b = request.POST['d_o_b']
            weight= request.POST['weight']
            height= request.POST['height']
            if name ==' ' or d_o_b == ' ' or weight ==' ' or height ==' ':
                return HttpResponse('name, d_o_b, weight, height are mandatory field', status=200)

            user_info = UserData(name=name, d_o_b= d_o_b, weight=weight, height= height)
            user_info.save()
            return HttpResponse('UserData successfully uplaoded', status=200)
        except Exception as e:
            return HttpResponse('Internal Server', status=400)


    def delete(self,request):
        try:
            username = request.GET.get('name')
            name =UserData.objects.filter(name__icontains=username)
            if name:
                user = UserData.objects.get(name = username)
                user.delete()
                return HttpResponse('Data successfully deleted' , status=200)

        except UserData.ObjectDoesNotExist as e:
            return HttpResponse('userdata not exists', status=400)
        except UserData.MultipleObjectsReturned as e:
            return HttpResponse('MultipleObjectsReturn', status=400)
        except Exception as e:
            return HttpResponse(e, status=400)


    def put(self,request):
        try:
			user_name = (request.GET.get('name'))
			entry = UserData.objects.get(name=user_name)

			userinfo = QueryDict(request.body)
			if not userinfo:
				return HttpResponse('Please provide something to change', status=200)

			if 'name' in userinfo.keys():
				entry.name = userinfo['name']
			if 'd_o_b' in userinfo.keys():
				entry.d_o_b = userinfo['d_o_b']
			if 'weight' in userinfo.keys():
				entry.weight = userinfo['weight']
			if 'height' in userinfo.keys():
				entry.height = userinfo['height']
			entry.save()
			return HttpResponse('Userinfo Updated Successfully', status=201)
        except UserData.DoesNotExist as e:
			return HttpResponse("User Not Found. Therefore cannot be updated", status=400)
        except Exception as e:
            return HttpResponse(e, status=400)


#top 5 orders
class TopOrder(View):
    def get(self,request):
        ans= []
        obj = Order.objects.all()
        for i in obj:
            obj_id = Order.objects.get(order_no = i.order_no)
            diet_names = obj_id.diet.all()
            for j in diet_names:
                ans.append(j.diet_name)
                ans.append(" ")
        res=[]
        res.append(Counter(ans).most_common(6))
        l = len(res)
        return HttpResponse(res, status=200)

    def post(self,request):
        try:
            diet_name = request.POST['diet_name']
            diet_cal =request.POST['diet_cal']
            diet_protein = request.POST['diet_protein']
            diet_fat = request.POST['diet_fat']
            diet_info = DietInfo(diet_name=diet_name, diet_cal=diet_cal, diet_protein=diet_protein, diet_fat=diet_fat)
            if diet_name ==' ' or diet_cal ==' ' or diet_fat ==' ' or diet_protein == ' ':
                return HttpResponse('diet_name, diet_cal, diet_protein, diet_fat cant be empty', status=200)
            diet_info.save()
            return HttpResponse('diet_info uploaded successfully',status=200)
        except Exception, e:
            return HttpResponse(e, status=400)

    def delete(self,request):
        try:
            dietname = request.GET.get('name')
            name =DietInfo.objects.get(diet_name=dietname)
            if name:
                name.delete()
                return HttpResponse('Data successfully deleted' , status=200)

        except DietInfo.DoesNotExist as e:
            return HttpResponse('diet not exists', status=400)
        except DietInfo.MultipleObjectsReturned as e:
            return HttpResponse('MultipleObjectsReturn', status=400)
        except Exception as e:
            return HttpResponse(e, status=400)

    def put (self,request):
        try:
            name = request.GET.get('name')
            entry = DietInfo.objects.get(diet_name=name)
            dietinfo = QueryDict(request.POST)
            if not dietinfo:
                return HttpResponse('Please provide something to change', status=200)

            if diet_name in dietinfo.keys():
                entry.diet_name = dietinfo['diet_name']
            if diet_cal in dietinfo.keys():
                entry.diet_cal = dietinfo['diet_cal']
            if diet_protein in dietinfo.keys():
                entry.diet_protein = dietinfo['diet_protein']
            if diet_fat in dietinfo.keys():
                entry.diet_fat = dietinfo['diet_fat']
            entry.save()
            return HttpResponse('Dietinfo Updated Successfully', status=201)
        except UserData.DoesNotExist as e:
			return HttpResponse("Diet Not Found. Therefore cannot be updated", status=400)
        except Exception as e:
            return HttpResponse(e, status=400)


# total_cal & total_fat &total_protein taken by a user.
class TotalDiet(View):
    def get(self,request):
        try:
            user_name = request.GET.get('User')
            order_obj = Order.objects.filter(user__name__icontains=user_name)
            if not order_obj:   #EMpty QUERYSET RETURNED
                return HttpResponse("User does not exists")
            #else:
            answer = []
            for val in order_obj:
                 ans = {}
                 #ans['user'] = val.user.name
                 order_obj = Order.objects.get(order_no = val.order_no)
                 diet_obj = order_obj.diet.all()
                 total_cal=0
                 total_fat=0
                 total_protein=0
                 for j in diet_obj:
                #print j.diet_name
                     total_protein += j.diet_protein
                     total_fat += j.diet_fat
                     total_cal += j.diet_cal
                 ans['total_cal'] = total_cal*7
                 ans['total_fat'] = total_fat*7
                 ans['total_protein'] = total_protein*7
                 answer.append(ans)
            return HttpResponse(json.dumps(answer), content_type='application/json', status=200)

        except Order.DoesNotExist, e:
            return HttpResponse('No Order exists for the given user.', status = 500)
        except Exception, e:
            return HttpResponse('Internal Server', status=500)

    #def post(self,request):
        #try:
